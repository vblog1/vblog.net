<?php
 
// Kết nối database và thông tin chung
require_once 'core/init.php';
 
// Nếu có tồn tại phương thức post
if (isset($_POST['user_signin']) && isset($_POST['pass_signin']))
{
    // Xử lý các giá trị 
    $user_signin = trim(htmlspecialchars(addslashes($_POST['user_signin'])));
    $pass_signin = trim(htmlspecialchars(addslashes($_POST['pass_signin'])));
 
    // Các biến xử lý thông báo
    $show_alert = '<script>$("#formSignin .alert").removeClass("hidden");</script>';
    $hide_alert = '<script>$("#formSignin .alert").addClass("hidden");</script>';
    $success = '<script>$("#formSignin .alert").attr("class", "alert alert-success");</script>';
 
    // Nếu giá trị rỗng
    if ($user_signin == '' || $pass_signin == '')
    {
        echo $show_alert.'Vui lòng điền đầy đủ thông tin.';
    }
    // Ngược lại
    else
    {
        //$sql_check_user_exist = "SELECT username FROM accounts WHERE username = '$user_signin'";
        $stmtClassOne = $db->prepare("SELECT username FROM accounts WHERE username = ? OR email = ?");
        $stmtClassOne->bind_param("ss",$user_signin,$user_signin);
        $stmtClassOne->execute();
        $resultClassOne = $stmtClassOne->get_result();
        // Nếu tồn tại username
        //if ($db->num_rows($sql_check_user_exist))
        if ($resultClassOne->num_rows > 0)
        {
            $pass_signin = md5($pass_signin);
            $stmtClassTwo = $db->prepare("SELECT username, password FROM accounts WHERE (username = ? OR email = ?) AND password = ?");
            $stmtClassTwo->bind_param("sss",$user_signin,$user_signin,$pass_signin);
            $stmtClassTwo->execute();
            $resultClassTwo = $stmtClassTwo->get_result();
           // if ($db->num_rows($sql_check_signin))
            if ($resultClassTwo->num_rows > 0)
            {
                $stmtClassThird = $db->prepare("SELECT username, password, status FROM accounts WHERE (username = ? OR email = ?) AND password = ? AND status = '0'");
                $stmtClassThird->bind_param("sss",$user_signin,$user_signin,$pass_signin);
                $stmtClassThird->execute();
                $resultClassThird = $stmtClassThird->get_result();
                // Nếu username và password khớp và tài khoản không bị khoá (status = 0)
                //if ($db->num_rows($sql_check_stt))
                if ($resultClassThird->num_rows > 0)
                {
                    // Lưu session
                    $session->send($user_signin);
                    $db->close(); // Giải phóng
                     
                    echo $show_alert.$success.'Đăng nhập thành công.';
                    echo 'Đăng nhập thành công.';
                   new Redirect($_DOMAIN); // Trở về trang index
                }
                else
                {
                    echo $show_alert.'Tài khoản của bạn đã bị khoá, vui lòng liên hệ quản trị viện để biết thêm thông tin chi tiết.';
                }
            }
            else
            {
                echo $show_alert.'Tên đăng nhập hoặc mật khẩu không chính xác.';
            }
        }
        // Ngược lại không tồn tại username
        else
        {
            echo $show_alert.'Tên đăng nhập hoặc mật khẩu không chính xác.';
        }
    }
}
// Ngược lại không tồn tại phương thức post
else
{
    new Redirect($_DOMAIN); // Trở về trang index
}
 
?>