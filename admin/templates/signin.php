<div class="container mt-5">
    <div class="row">
        <div class="col-md-3">
        </div> <!-- div.null-->
        <div class="col-md-6 bg-dark py-5 px-5">
            <h2 class="text-center mt-3">Vui lòng đăng nhập để tiếp tục.</h2>
            <form method="POST" id="formSignin" onsubmit="return false;">
                <div class="form-group mt-5">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                        <input type="text" class="form-control py-5 txt-lg" placeholder="Tên đăng nhập" id="user_signin">
                    </div>
                </div>
                <div class="form-group mt-5">
                    <div class="input-group ">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" class="form-control py-5 txt-lg" placeholder="Mật khẩu" id="pass_signin">
                    </div>
                </div>
                <div class="form-group mt-5">
                    <button type="submit" id='formSignin-btn' class="btn btn-primary py-5 form-control">Đăng nhập</button>
                </div>
                <div class="alert alert-danger hidden"></div>
            </form>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>