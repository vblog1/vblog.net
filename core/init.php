<?php
 
// Require các thư viện PHP
require 'admin/classes/DB.php';
require 'admin/classes/Session.php';
require 'admin/classes/Functions.php';
 
// Kết nối database
$db = new DB();
$db->connect();
$db->set_char('utf8');

$_DOMAIN = 'https://viewblog.net/';
// Lấy thông tin website
$sql_get_data_web = "SELECT * FROM website";
if ($db->num_rows($sql_get_data_web)) {
    $data_web = $db->fetch_assoc($sql_get_data_web, 1);
}
 
?>